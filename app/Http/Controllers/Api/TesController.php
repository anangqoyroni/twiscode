<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\TransaksiHeader;
use App\TransaksiDetail;
use App\User;
use Log;
use JWTAuth;

class TesController extends Controller
{
    public function binaryToDecimal(Request $request)
    {
        $number = $request->input('number');
        $digits = str_split($number);
        $reversed = array_reverse($digits);
        $result = 0;

        for($x=0; $x < count($reversed); $x++) {
            if($reversed[$x] == 1) {
                $result += pow(2, $x);
            }
        }
        return response()->json([
            'result' => $result
        ]);
    }

    public function decimalToBinary(Request $request)
    {
        $decimal = $request->input('decimal');
        $binaryNum; 
        $i = 0; 
        while ($decimal > 0)  
        { 
            $binaryNum[$i] = $decimal % 2; 
            $decimal = (int)($decimal / 2); 
            $i++; 
        } 

        for ($j = $i - 1; $j >= 0; $j--) {
            $binaryNum[$j];
        }
        $result = implode("", array_reverse($binaryNum)); 
        $message = 'Username or Password incorrect';
        return response()->json([
            'result' => $result
        ]);
    }

    public function query(Request $request)
    {
        $data = TransaksiHeader::join('transaksi_detail', 'transaksi_detail.id_transaksi', '=', 'transaksi_header.id')
        ->selectRaw('transaksi_header.id, tanggal_order,status_pelunasan,tanggal_pembayaran,tanggal_pembayaran,sum(subtotal) AS total,sum(jumlah) AS jumlah_barang')
        ->groupBy('transaksi_detail.id_transaksi')
        ->get();
        
        return response()->json([
            'result' => $data
        ]);
    }
}
