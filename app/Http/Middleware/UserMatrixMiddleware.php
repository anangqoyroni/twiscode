<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Model\Setting\UserAccess;
use Closure;

class UserMatrixMiddleware {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $module_function_id) {
        $module_function_id_array = explode(';', $module_function_id);
        if (!Auth::guard('web')->check()) {
            return redirect('/login');
        }
        // dd(session()->all());
        $user_access = UserAccess::where('user_id',Auth::user()->id)->get();

        $arrAccess = [];
        foreach ($user_access as $u) {
            $arrAccess[] = $u->module_function_id;
        }
        // dd($arrAccess);
        $ada=false;
        foreach ($module_function_id_array as $moduleID) {
            if (!in_array($moduleID, $arrAccess)) {
                if(!$ada)
                    $ada=false;
            }
            else
            {
                $ada=true;
            }
        }
        if(!$ada)
        {
            abort(401, 'Unauthorized user.');
            // return back()->with('unauthorized', 'Unauthorized user.');
        }
        return $next($request);
    }

}
