<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiDetail extends Model
{	
	protected $table = 'transaksi_detail';
	protected $primaryKey = 'id';
	public $incrementing = true;
    protected $fillable = [
        'id','id_transaksi',
        'harga','jumlah',
        'subtotal',
        'created_at','updated_at'
    ];
}
