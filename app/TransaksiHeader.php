<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiHeader extends Model
{	
	protected $table = 'transaksi_header';
	protected $primaryKey = 'id';
	public $incrementing = true;
    protected $fillable = [
        'id','tanggal_order',
        'status_pelunasan','tanggal_pembayaran',
        'created_at','updated_at'
    ];
}
