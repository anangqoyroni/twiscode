<?php
	function array_has_dupes($array) {
       // streamline per @Felix
       return count($array) !== count(array_unique($array));
    }
?>