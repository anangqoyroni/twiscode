<?php

use Illuminate\Database\Seeder;

class TransaksiDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('transaksi_detail')->insert([
            'id' => 1,
            'id_transaksi' => 1,
            'harga' => '5000',
            'jumlah' => '2',
            'subtotal' => '10000',
            'created_at' => '2021-06-30 01:01:01',
            'updated_at' => '2021-06-30 01:01:01',
        ]);
    }
}
