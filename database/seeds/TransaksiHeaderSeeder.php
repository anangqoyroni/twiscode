<?php

use Illuminate\Database\Seeder;

class TransaksiHeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transaksi_header')->insert([
            'id' => 1,
            'tanggal_order' => '2021-06-29',
            'status_pelunasan' => 'lunas',
            'tanggal_pembayaran' => '2021-06-30',
            'created_at' => '2021-06-30 01:01:01',
            'updated_at' => '2021-06-30 01:01:01',
        ]);
    }
}
