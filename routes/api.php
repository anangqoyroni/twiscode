<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('binary-to-decimal', 'Api\TesController@binaryToDecimal')->name('binary-to-decimal');
Route::post('decimal-to-binary', 'Api\TesController@decimalToBinary')->name('decimal-to-binary');
Route::get('query', 'Api\TesController@query')->name('query');
