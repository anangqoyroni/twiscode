const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .sass('resources/sass/app.scss', 'public/css');

mix.styles([
	// 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons',
	// 'https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css',
	// 'public/assets/css/material-dashboard.css',
	// 'public/assets/css/style.css',
	// 'public/assets/vendor/datatables/datatables.min.css',
	// 'public/assets/vendor/chart-js/chart.css',
], 
'public/css/styles.css');

mix.scripts([
	// 'public/assets/js/core/jquery.min.js',
	// 'public/assets/js/core/popper.min.js',
	// 'assets/assets/js/core/bootstrap-material-design.min.js',
	// 'assets/assets/js/plugins/perfect-scrollbar.jquery.min.js',
	// 'public/assets/js/plugins/moment.min.js',
	// 'public/assets/js/plugins/sweetalert2.js',
	// 'public/assets/js/plugins/jquery.validate.min.js',
	// 'public/assets/js/plugins/jquery.bootstrap-wizard.js',
	// 'public/assets/js/plugins/bootstrap-selectpicker.js',
	// 'public/assets/js/plugins/bootstrap-datetimepicker.min.js',
	// 'public/assets/js/plugins/jquery.dataTables.min.js',
	// 'public/assets/js/plugins/bootstrap-tagsinput.js',
	// 'public/assets/js/plugins/jasny-bootstrap.min.js',
	// 'public/assets/js/plugins/fullcalendar.min.js',
	// 'public/assets/js/plugins/jquery-jvectormap.js',
	// 'public/assets/js/plugins/nouislider.min.js',
	// 'public/assets/js/core/core.js',
	// 'public/assets/js/plugins/arrive.min.js',
	// 'public/assets/js/plugins/bootstrap-notify.js',
	// 'public/assets/js/material-dashboard.js',
	// 'public/assets/vendor/datatables/datatables.min.js',
	// 'public/assets/vendor/chart-js/chart.bundle.min.js',
	// 'public/assets/vendor/chart-js/chart.min.js',
	// 'public/assets/js/default-app.js',
],
'public/js/scripts.js');
